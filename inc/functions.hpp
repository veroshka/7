#pragma once
namespace ver
{
    void Read(int& matrixSize, int mas[100][100])
    {
        std::cin >> matrixSize;
        for (int i = 0; i < matrixSize; i++)
            for (int j = 0; j < matrixSize; j++)
                std::cin >> mas[i][j];
    }
}

int maximum(int matrixSize, int mas[100][100])
{
    int max = mas[0][0];
    for (int i = 0; i < matrixSize; i++)
        for (int j = 0; j < matrixSize; j++)
            if (mas[i][j] > max)
            {
                max = mas[i][j];
            }

    return max;
}

int min(int matrixSize, int mas[100][100])
{
    int min = mas[0][0];
    for (int i = 0; i < matrixSize; i++)
        for (int j = 0; j < matrixSize; j++)
            if (mas[i][j] < min)
            {
                min = mas[i][j];
            }

    return min;
}

bool isEightInThisElement(int a)
{
    while (a > 0)
    {
        if (a % 10 == 8)
        {
            return true;
        }
        else
        {
            a /= 10;
        }
    }
    return false;
}

bool isEightInThisRow(int rowSize, int mas[100])
{
    for (int i = 0; i < rowSize; i++)
    {
        if (isEightInThisElement(mas[i]))
        {
            return true;
        }
    }
    return false;
}

void Sort(int matrixSize, int matrix[100])
{
    for (int i = 0; i < matrixSize - 1; i++)
        for (int j = i + 1; j < matrixSize; j++)
            if (matrix[i] >= matrix[j])
            {
                std::swap(matrix[i], matrix[j]);
            }
}

void Write(int matrixSize, int mas[100][100])
{
    std::cout << matrixSize << std::endl;
    for (int i = 0; i < matrixSize; i++)
        for (int j = 0; j < matrixSize; j++)
            std::cout << mas[i][j] << " ";
}
